package colecciones;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class EjemplosColecciones {
	
	
	public static void main(String[] args) {
		
		// Set no acepta repetidos - List sí lo hace
		Collection<String> c = new TreeSet<>();
		
		c.add("Alberto");
		c.add("Diana");
		c.add("Alberto");
		
		for (String s: c) {
			System.out.println(s);
		}

		/*
		Set<Persona> personas = new TreeSet<>();
		personas.add(new Persona("Juanita"));
		for (Persona p: personas) {
			System.out.println(p.getNombre());
		}
		*/
		
		
		Persona[] ellos = {
				new Persona("Marcela"), 
				new Persona("Andrea"),
				new Persona("Antonio"),
				new Persona("Marcela"),
		}; 
		
		Map<Persona, Integer> menciones = new HashMap<>();
		
		for (Persona p: ellos) {
			Integer cantidad = menciones.get(p);
			if (cantidad == null)
				cantidad = 0;
			cantidad ++;
			menciones.put(p, cantidad);
		}
		
		for (Persona p: menciones.keySet()) {
			System.out.println(p.getNombre() + " " + menciones.get(p));
		}
		
		for (Persona p: ellos) {
			System.out.println(p.getNombre() + " " + p);
		}
		
		
	}

}

class Persona {
	
	private String nombre;
	
	public Persona(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
	
}
