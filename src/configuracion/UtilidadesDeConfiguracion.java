package configuracion;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class UtilidadesDeConfiguracion {
	
	// https://docs.oracle.com/javase/tutorial/essential/environment/config.html
	public static void main(String[] args) {
		String cadena = "Hola!";
		int cantidad =  1;
		
		// ---- Argumentos por la línea de comandos ----
		if (args.length >= 2) {  //Siempre controlar que se pueda acceder
			cadena = args[0];
			cantidad = aEntero(args[1]);
		} // Si no se puede acceder tomar una acción adecuada - en este caso ignora los parámetros-
		
		ejecutar (cantidad, cadena, "Argumentos por la línea de comandos");

		//---------------
		
		
		// ---- Archivo de propiedades ----
		cadena = "Hola!"; cantidad =  1;
		
		Properties prop = getProperties();
		cadena = prop.getProperty("cadena");
		cantidad = aEntero(prop.getProperty("cantidad"));
		
		ejecutar (cantidad, cadena, "Archivo de propiedades");
		// En este ejemplo sólo leemos pero se pueden establecer y guardar 
		// programáticmante en tiempo de ejecución 
		
		//---------------		
		
		// ---- Variables de ambiente ----
		cadena = "Hola!"; cantidad =  1;
		
		cadena = System.getenv().get("CADENA");
		cantidad = aEntero(System.getenv().get("CANTIDAD"));
		
		ejecutar (cantidad, cadena, "Variables de ambiente");
		
		
	}
	
	
	public static void ejecutar (int cantidad, String cadena, String origen) {
		System.out.println("Con valores obtenidos desde: " + origen);
		for (int i= 0; i < cantidad; i++ ) {
			System.out.print(cadena + " ");
		}
		System.out.println("\n");
	}	
	
	private static Properties getProperties () {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("propiedades_del_proyecto.properties"));
		} catch(IOException e) {
			System.out.println(e.toString());
		}	
		return prop;
	}
	
	private static int aEntero(String s) {
		if (s == null)
			return 1;
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			System.out.println(e.toString());
			return 1;
		}
	}
	
}

