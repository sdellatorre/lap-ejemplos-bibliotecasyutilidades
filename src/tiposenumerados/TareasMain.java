package tiposenumerados;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import tiposenumerados.Tarea.Prioridad;

public class TareasMain {
	
	public static Random rnd = new Random();
	
	public static void main(String[] args) {
		
		List<Tarea> tareas = new LinkedList<Tarea>();
 		
		for (int i=1 ; i <= 10 ;i ++) {
			Tarea tarea = new Tarea("Tarea " + i);
			if (rnd.nextInt() > 0.3) {
				tarea.setPrioridad(Prioridad.MEDIA);
			} else if (rnd.nextInt() > 0.3) { 
				tarea.setPrioridad(Prioridad.ALTA);
			}
			tareas.add(tarea);
		}
		
		for (Tarea tarea: tareas) {
			System.out.println(tarea.getDescripcion() + " " + tarea.getPrioridad());
		}
		System.out.println();
		
		//Se puede iterar sobre los elementos del tipo
		for(Prioridad pr: Tarea.Prioridad.values()) {
			System.out.println(pr.toString()  +" name    " + pr.name());
			System.out.println(pr.toString()  +" ordinal " + pr.ordinal());
		}
		
		Tarea tareaA = tareas.get(rnd.nextInt(10));
		Tarea tareaB = tareas.get(rnd.nextInt(10));
		
		if (tareaA.getPrioridad().compareTo(tareaB.getPrioridad()) > 0 ) {
			System.out.printf("La tarea %s tiene más prioridad que la tarea %s\n", tareaA.getDescripcion(), tareaB.getDescripcion());
		} else if (tareaA.getPrioridad().compareTo(tareaB.getPrioridad()) < 0 ) {
			System.out.printf("La tarea %s tiene menos prioridad que la tarea %s\n", tareaA.getDescripcion(), tareaB.getDescripcion());
		} else {
			System.out.printf("La tarea %s la misma prioridad que la tarea %s\n", tareaA.getDescripcion(), tareaB.getDescripcion());
		}
		
		switch (tareas.get(0).getPrioridad()) {
			case ALTA: System.out.println("ALTA"); break;
			case MEDIA: System.out.println("MEDIA"); break;
			case BAJA: System.out.println("BAJA"); break;
		}
		
		
	}

}
