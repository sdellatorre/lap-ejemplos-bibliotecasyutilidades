package tiposenumerados;

// https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
public enum Provincia {
	NEUQUEN ("Neuquén", 94_078),
	RIO_NEGRO ("Viedma", 203_013),
	CHUBUT ("Raswon", 224_686),
	SANTA_CRUZ ("Río Gallegos", 243_943),
	TIERRA_DEL_FUEGO("Ushuaia", 1_002_445);
	
	private String capital;
	private int superficie;
	
	private Provincia(String capital, int superficie) {
		this.capital = capital;
		this.superficie = superficie;
	}

	public String getCapital() {
		return capital;
	}
	
	public int getSuperficie() {
		return superficie;
	}
	
}
