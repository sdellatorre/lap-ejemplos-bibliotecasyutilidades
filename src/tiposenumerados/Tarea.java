package tiposenumerados;

public class Tarea {
	/* 
	 * 	public static final int PRIORIDAD_BAJA = 0;
	 *  public static final int PRIORIDAD_MEDIA = 1;
	 *  public static final int PRIORIDAD_ALTA = 2;
	 */
	
	public enum Prioridad {
		BAJA,
		MEDIA,
		ALTA
	}
	
	private String descripcion;
	private Prioridad prioridad;
	
	public Tarea(String descripcion) {
		this.descripcion = descripcion;
		this.prioridad = Prioridad.BAJA;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public Prioridad getPrioridad() {
		return prioridad;
	}
	
	public void setPrioridad(Prioridad prioridad) {
		this.prioridad = prioridad;
	}
	
}
