package comparadores;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class GrupoEscolar implements Comparable <GrupoEscolar> {
	
	private int grado;
	private char division;
	
	
	public GrupoEscolar(int grado, char division) {
		super();
		this.grado = grado;
		this.division = division;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return grado +"° "+ division;
	}

	@Override
	public int compareTo(GrupoEscolar otroGrupo) {
		if (this.grado == otroGrupo.grado)
			return this.division - otroGrupo.division;
		return this.grado - otroGrupo.grado;
	}
	
	
	public static void main(String[] args) {
		
		List<GrupoEscolar>  grupos =  new LinkedList<GrupoEscolar>(); 
		
		for (char division = 'B'; division >= 'A' ; division--) {
			for (int grado = 1 ; grado <= 6 ; grado++) {
				grupos.add(new GrupoEscolar(grado, division));
			}
		}
		
		System.out.println("--Desordenados");
		for (GrupoEscolar grupo: grupos) {
			System.out.println(grupo);
		}
		
		Collections.sort(grupos);
		
		System.out.println("--Ordenados");
		for (GrupoEscolar grupo: grupos) {
			System.out.println(grupo);
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + division;
		result = prime * result + grado;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoEscolar other = (GrupoEscolar) obj;
		if (division != other.division)
			return false;
		if (grado != other.grado)
			return false;
		return true;
	}
	
	
	
}
