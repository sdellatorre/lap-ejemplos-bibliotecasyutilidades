package comparadores;

import java.util.Comparator;

public class PersonaPorEdadComparator implements Comparator<Persona> {
	@Override
	public int compare(Persona p1, Persona p2) {
		return p1.getFechaNacimiento().compareTo(p2.fechaNacimiento);
	}
}


