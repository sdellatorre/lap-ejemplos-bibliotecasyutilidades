package comparadores;

import java.util.Comparator;

public class PersonaPorNombreComparator  implements Comparator<Persona> {
	@Override
	public int compare(Persona p1, Persona p2) {
		if (p1.getNombre().equals(p2.getNombre()))
			return p1.getApellido().compareTo((p2.getApellido()));
		return p1.getNombre().compareTo(p2.getNombre());
	}
}