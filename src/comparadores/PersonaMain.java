package comparadores;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PersonaMain {
	
	public static void main(String[] args) {
		
		List <Persona> personas = cargarPersonas();
		
		Collections.shuffle(personas);
		System.out.println("---- Al azar");
		for (Persona p: personas) {
			System.out.println(p);
		}
		
		//Collections.sort(personas);
		
		Collections.sort(personas, new PersonaPorApellidoComparator());
		System.out.println("---- Por apellido");
		for (Persona p: personas) {
			System.out.println(p);
		}
		
		Collections.sort(personas, new PersonaPorNombreComparator());
		System.out.println("---- Por Nombre");
		for (Persona p: personas) {
			System.out.println(p);
		}
		
		Collections.sort(personas, new PersonaPorEdadComparator());
		Collections.reverse(personas);
		System.out.println("---- Por Edad (primero los más jóvenens)");
		for (Persona p: personas) {
			System.out.println(p);
		}
		
	}
	
	
	public static List<Persona> cargarPersonas() {
		List<Persona> personas = new LinkedList<>();
	
		personas.add(new Persona("ANDREANI",  "NELSON",  LocalDate.of(1975, Month.DECEMBER, 1)));
		personas.add(new Persona("ANDREANI",  "JAVIER",  LocalDate.of(1978, Month.APRIL, 22)));
		personas.add(new Persona("RODRIGUEZ", "RAQUEL",  LocalDate.of(1985, Month.MAY, 14)));
		personas.add(new Persona("BARRERA",   "ALBERTO", LocalDate.of(1962, Month.AUGUST, 7)));
		personas.add(new Persona("GARCIA",    "SERENA",  LocalDate.of(1997, Month.NOVEMBER, 19)));
		personas.add(new Persona("ZURITA",    "CAMILO",  LocalDate.of(2001, Month.NOVEMBER, 30)));
		
		return personas;
	}
	

}
